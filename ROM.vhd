--*****************************************************************************
-- ROM.vhd
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
--	Description:
--		1 port 1 clock ROM for storing the final software
--*****************************************************************************
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.global_types.all;

entity ROM is
    generic 
    (
        size : integer := 268 / 2
    );
    port (
        reset_n : in std_logic;
        CLOCK : in std_logic;
        addr_in : in std_logic_vector(15 downto 0);
        data_out : out std_logic_vector(15 downto 0)
    );
end entity ROM;

architecture rtl of ROM is                            
    signal ROM_CONTENT : array16(size-1 downto 0) := (X"0100",X"0000",X"0A00",X"0100",X"0200",X"0300",X"2700",X"0400",X"0400",X"0700",X"0100",X"0801",X"0F00",X"0100",X"1800",X"2700",X"0300",X"0300",X"1500",X"0300",X"3000",X"0D00",X"0300",X"0100",X"0D00",X"0100",X"0100",X"1D00",X"0000",X"0100",X"0200",X"3700",X"2400",X"0D00",X"0400",X"0100",X"0D00",X"0000",X"0100",X"1500",X"0400",X"0400",X"3700",X"1200",X"1C00",X"0100",X"0000",X"3201",X"0100",X"0200",X"0300",X"2700",X"0400",X"0400",X"0700",X"0100",X"0A01",X"0F00",X"0100",X"1800",X"2700",X"0300",X"0300",X"1500",X"0300",X"3000",X"0D00",X"0300",X"0100",X"0D00",X"0100",X"0100",X"1D00",X"0000",X"0100",X"0200",X"3700",X"7E00",X"0D00",X"0400",X"0100",X"0D00",X"0000",X"0100",X"1500",X"0400",X"0400",X"3700",X"6C00",X"1C00",X"3E00",X"0300",X"0DB0",X"0900",X"FC00",X"0100",X"0000",X"0000",X"0100",X"0100",X"0000",X"0100",X"0200",X"0000",X"1D00",X"0000",X"0100",X"0200",X"0D00",X"0000",X"0100",X"1500",X"0000",X"4001",X"3700",X"D000",X"0D00",X"0100",X"0100",X"1500",X"0100",X"F000",X"3700",X"D000",X"0900",X"FC00",X"1A00",X"0000",X"1A00",X"5A00",X"0900",X"FC00",X"7800",X"7800");
begin
    -- Core --
    process( CLOCK, reset_n )
    begin
        if reset_n = '0' then
            data_out <= (others => '0');
        elsif rising_edge( CLOCK ) then
            -- flip byte order to make everything simpler
            data_out <= ROM_CONTENT((size-1)-to_integer(unsigned(addr_in)))(7 downto 0) & ROM_CONTENT((size-1)-to_integer(unsigned(addr_in)))(15 downto 8);
        end if;
    end process;
    
end;

