-- Engineering job top level 
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description:
--  
-- Top level entity

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.global_types.all;

entity vhdl_engineering_job is
    port 
    (
        CLOCK_50 : in std_logic;
        reset_n : in std_logic;
        KEY_in : in std_Logic_vector(3 downto 0);
        VGA_VS : out std_logic; 
        VGA_HS : out std_logic;
        VGA_BLANK_N : out std_logic;
        VGA_RGB : out std_Logic_vector(23 downto 0);
        VGA_CLK : out std_logic;
        segment1 : out std_logic_vector(6 downto 0);
        segment2 : out std_logic_vector(6 downto 0);
        segment3 : out std_logic_vector(6 downto 0);
        segment4 : out std_logic_vector(6 downto 0);
        segment5 : out std_logic_vector(6 downto 0);
        LED_OUT : out std_logic_vector(9 downto 0)
    );

end vhdl_engineering_job;

architecture rtl of vhdl_engineering_job is 

    component altera_fifo IS
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q		    : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
    end component altera_fifo;

    component vJTAG is
		port (
			tdi                : out std_logic;                                       -- tdi
			tdo                : in  std_logic                    := 'X';             -- tdo
			ir_in              : out std_logic_vector(0 downto 0);                    -- ir_in
			ir_out             : in  std_logic_vector(0 downto 0) := (others => 'X'); -- ir_out
			virtual_state_cdr  : out std_logic;                                       -- virtual_state_cdr
			virtual_state_sdr  : out std_logic;                                       -- virtual_state_sdr
			virtual_state_e1dr : out std_logic;                                       -- virtual_state_e1dr
			virtual_state_pdr  : out std_logic;                                       -- virtual_state_pdr
			virtual_state_e2dr : out std_logic;                                       -- virtual_state_e2dr
			virtual_state_udr  : out std_logic;                                       -- virtual_state_udr
			virtual_state_cir  : out std_logic;                                       -- virtual_state_cir
			virtual_state_uir  : out std_logic;                                       -- virtual_state_uir
			tck                : out std_logic                                        -- clk
		);
	end component vJTAG;

    -- jtag signals 
    signal vJTAG_tck : std_logic := '0';
    signal vJTAG_tdi : std_logic := '0';
    signal vJTAG_ir_in : std_logic := '0';
    signal vJTAG_sdr : std_logic := '0';
    signal vJTAG_udr : std_logic := '0';
    signal vJTAG_tdo : std_logic := '0';
    signal vJTAG_out_clk : std_logic := '0';
    signal vJTAG_fifo_we : std_logic := '0';
    signal vJTAG_out_data : std_logic_vector(15 downto 0);

    -- reset signal 
    signal reset_signal : std_logic := '0';
    signal memory_clock : std_logic := '0';
         
    -- metastable  
    signal KEY_stable : std_logic_vector(3 downto 0) := (others => '0');
    signal reset_stable : std_logic := '1';
    signal reversed_reset : std_logic := '0';

    -- cpu
    signal cpu_port_signal : integer range 0 to 8;
    signal cpu_data_in_signal : std_logic_vector(15 downto 0) := (others => '0');
    signal cpu_data_out_signal : std_logic_vector(15 downto 0) := (others => '0');
    signal cpu_addr_out_signal : std_logic_vector(15 downto 0) := (others => '0');
    signal cpu_we : std_logic := '0';
    
    -- fifo connected to vJTAG and CPU 
    signal fifo_clock : std_logic := '0'; -- fifo pop_clock clock from cpu
    signal fifo_empty : std_logic := '1'; 
    signal fifo_full : std_logic := '0';
    signal fifo_data : std_logic_vector(15 downto 0) := (others => '0');
    signal fifo_read_enable : std_logic := '0';
    
    -- gpu
    signal gpu_addr_signal : std_Logic_vector(15 downto 0) := (others => '0');
    signal gpu_data_signal : std_Logic_vector(15 downto 0) := (others => '0');
    signal gpu_we_signal : std_logic := '0';
    signal gpu_data_out_signal : std_Logic_vector(15 downto 0) := (others => '0');

    -- led 
    signal led_data_out_signal : std_Logic_vector(15 downto 0) := (others => '0');
    signal led_data_in_signal : std_Logic_vector(15 downto 0) := (others => '0');

    -- vga
    signal vga_addr_signal : std_Logic_vector(16 downto 0) := (others => '0');
    signal vga_data_signal : std_Logic_vector(5 downto 0) := (others => '0');
    signal vga_we_signal : std_logic := '0';
    signal vga_data_out_signal : std_Logic_vector(5 downto 0) := (others => '0');

    -- ram
    signal ram_addr_in : std_logic_vector(15 downto 0) := (others => '0');
    signal ram_data_in : std_logic_vector(15 downto 0) := (others => '0');
    signal ram_data_out : std_logic_vector(15 downto 0) := (others => '0');
    signal ram_we : std_logic := '0';

    -- rom
    signal rom_addr_in : std_logic_vector(15 downto 0) := (others => '0');
    signal rom_data_out : std_logic_vector(15 downto 0) := (others => '0');
    
    -- bcd 

    signal bcd_number   : std_logic_vector(15 downto 0);
	signal bcd_decimals : std_logic_vector(19 downto 0);

    -- signals to connect to bus
    signal bus_device_addr_out : array16(7 downto 0) := (others => (others => '0'));
    signal bus_device_data_out : array16(7 downto 0) := (others => (others => '0'));
    signal bus_device_data_in : array16(7 downto 0) := (others => (others => '0'));
    signal bus_device_we_out : std_Logic_vector(7 downto 0) := (others => '0');

begin
    
    -- map devices to bus ports here

    -- GPU 0 --
    gpu_addr_signal <= bus_device_addr_out(0);
    gpu_data_signal <= bus_device_data_out(0); 
    bus_device_data_in(0) <= gpu_data_out_signal;
    gpu_we_signal <= bus_device_we_out(0);
    -- RAM 1
    ram_addr_in <= bus_device_addr_out(1);
    ram_data_in <= bus_device_data_out(1); 
    bus_device_data_in(1) <= ram_data_out;
    ram_we <= bus_device_we_out(1);
    -- ROM 2
    rom_addr_in <= bus_device_addr_out(2);
    bus_device_data_in(2) <= rom_data_out;
    -- BCD 3 - 7 segment display
    LED_OUT <= fifo_data(9 downto 0); 
    bcd_number <= bus_device_data_out(3);
    -- KEY INPUT 4
    bus_device_data_in(4) <= "000000000000" & key_stable;

    vga_component_inst : entity work.vga_component
    port map(CLOCK_50, reset_signal, VGA_CLK, VGA_VS, VGA_HS, VGA_BLANK_N, VGA_RGB, vga_addr_signal, vga_data_signal, vga_we_signal, vga_data_out_signal);
    
    metastability_inst : entity work.metastability
    generic map(4)
    port map (CLOCK_50, reset_n, reset_stable, KEY_in, KEY_stable);
    
    -- CPU instance 
    cpu_inst : entity work.CPU
    port map(
        CLOCK_50,
        reset_signal,
        cpu_port_signal,
        cpu_data_out_signal,
        cpu_addr_out_signal,
        cpu_we,
        cpu_data_in_signal,
        fifo_data,
        fifo_full,
        fifo_empty,
        fifo_clock,
        fifo_read_enable
        );

    -- LED out 
    --led_inst : entity work.LED_OUT
    --port map(CLOCK_50, reset_stable, bus_device_data_out(3), led_data_out_signal);

    -- GPU instance 
    gpu_inst : entity work.GPU
    port map(
        CLOCK_50,
        fifo_clock,
        reset_signal,
        gpu_addr_signal,
        gpu_data_signal,
        gpu_we_signal,
        gpu_data_out_signal,
        vga_addr_signal,
        vga_data_signal,
        vga_we_signal,
        vga_data_out_signal
    );

    -- RAM 
    ram_inst : entity work.RAM
    port map(
        CLOCK_50,
        reset_signal,
        ram_we,
        ram_addr_in,
        ram_data_in,
        ram_data_out
    );

    -- ROM 
    rom_inst : entity work.ROM 
    port map(
        reset_signal,
        CLOCK_50,
        rom_addr_in,
        rom_data_out
    );
    
    -- data bus
    data_bus_inst : entity work.Data_bus
    port map(
        reset_n => reset_signal,
        CLOCK => CLOCK_50,
        port_select => cpu_port_signal, 
        we_in => cpu_we,
        -- connect devices --
        devices_addr_out => bus_device_addr_out,
        devices_data_out => bus_device_data_out,
        devices_data_in => bus_device_data_in,
        devices_we_out => bus_device_we_out,
        -- connect CPU 
        addr_in => cpu_addr_out_signal, 
        data_in => cpu_data_out_signal, 
        data_out => cpu_data_in_signal
    );

    altera_fifo_inst : altera_fifo
    port map(
        reversed_reset,
        vJTAG_out_data,
        fifo_clock,
        fifo_read_enable,
        vJTAG_out_clk,
        vJTAG_fifo_we,
        fifo_data,
        fifo_empty,
        fifo_full
    );

    -- jtag interface 
    vJTAG_interface_inst : entity work.VJTAG_interface 
    port map(
        vJTAG_tck,
        vJTAG_tdi,
        vJTAG_ir_in,
        vJTAG_sdr,
        vJTAG_udr,
        vJTAG_tdo,
        vJTAG_out_data, -- data goes to fifo stack 
        vJTAG_out_clk, -- clock to push data to fifo stack
        vJTAG_fifo_we -- write enable for fifo 
    );

    vJTAG_inst : component vJTAG
    port map (
        tdi                => vJTAG_tdi,                -- jtag.tdi
        tdo                => vJTAG_tdo,                --     .tdo
        ir_in(0)              => vJTAG_ir_in,              --     .ir_in
        ir_out             => open,             --     .ir_out
        virtual_state_cdr  => open,  --     .virtual_state_cdr
        virtual_state_sdr  => vJTAG_sdr,  --     .virtual_state_sdr
        virtual_state_e1dr => open, --     .virtual_state_e1dr
        virtual_state_pdr  => open,  --     .virtual_state_pdr
        virtual_state_e2dr => open, --     .virtual_state_e2dr
        virtual_state_udr  => vJTAG_udr,  --     .virtual_state_udr
        virtual_state_cir  => open,  --     .virtual_state_cir
        virtual_state_uir  => open,  --     .virtual_state_uir
        tck                => vJTAG_tck                 --  tck.clk
    );

    reversed_reset <= not reset_stable;
        
    -- Reset signal --
    process( CLOCK_50 )
        variable clock_counter : integer := 0;
        begin
        if rising_edge( CLOCK_50 ) then
            if reset_stable = '0' then 
                clock_counter := 0;
                reset_signal <= '0';
            else 
                if clock_counter < 5 then 
                    clock_counter := clock_counter + 1;
                    reset_signal <= '0';
                else
                    reset_signal <= '1';
                end if;
            end if;
        end if;
    end process;

    ----- segment7 -----

    c2 : entity work.segment7 port map
    (
        CLOCK_50,
        bcd_number(3  downto  0),
        segment1
    );
    
    c3 : entity work.segment7 port map
    (
        CLOCK_50,
        bcd_number(7  downto  4),
        segment2
    );
    
    c4 : entity work.segment7 port map
    (
        CLOCK_50,
        bcd_number(11 downto  8),
        segment3
    );
    
    c5 : entity work.segment7 port map
    (
        CLOCK_50,
        bcd_number(15 downto 12),
        segment4
    );
    
    c6 : entity work.segment7 port map
    (
        CLOCK_50,
        X"0",
        segment5
    );
    
end rtl;