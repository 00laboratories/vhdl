-- VGA Component 
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description:
--  component combines vga core with memory 
--

library ieee;
use ieee.std_logic_1164.all;

entity vga_component is
    port 
    (
        CLOCK_50 : in std_logic;
        reset_n : in std_logic;
        VGA_CLK : out std_logic;
        VGA_VS : out std_logic; 
        VGA_HS : out std_logic;
        VGA_BLANK_N : out std_logic;
        VGA_RGB : out std_Logic_vector(23 downto 0);
        -- memory access for outside component 
        vga_mem_addr : in std_logic_vector(16 downto 0);
        vga_mem_data : in std_logic_vector(5 downto 0);
        vga_mem_we : in std_logic;
        vga_mem_data_out : out std_logic_vector(5 downto 0)
    );

end vga_component;

architecture rtl of vga_component is 
    component PLL
        port (
            refclk   : in  std_logic := '0'; --  refclk.clk
            rst      : in  std_logic := '0'; --   reset.reset
            outclk_0 : out std_logic         -- outclk0.clk
        );
    end component PLL;

    constant vga_mem_size : integer := 320 * 240; -- 76800 pixels
    constant vga_mem_addr_width : integer := 17;
    constant vga_data_width : integer := 6; -- 2 bits per pixel
            
    signal vga_addr_signal_a : std_logic_vector(vga_mem_addr_width-1 downto 0);
    signal vga_data_signal_a : std_logic_vector(vga_data_width-1 downto 0);
    
    signal VGA_CLK_signal : std_logic;
    
begin

    VGA_CLK <= vga_CLK_signal;
    
    PLL_inst : PLL 
    port map(CLOCK_50, '0', VGA_CLK_signal);
    
    vga_core_inst : entity work.vga_core
    generic map 
    (
        addr_width => vga_mem_addr_width,
        data_width => vga_data_width
    )
    port map
    (
        reset_n => reset_n,
        VGA_data_in => vga_data_signal_a,
        VGA_addr_out => vga_addr_signal_a,
        VGA_CLK => VGA_CLK_signal,
        VGA_VS => VGA_VS,
        VGA_HS => VGA_HS,
        VGA_BLANK_N => VGA_BLANK_N,
        VGA_RGB => VGA_RGB
    );    
        
    vga_memory_inst : entity work.vga_memory 
    generic map
    (
        size => vga_mem_size, 
        addr_width => vga_mem_addr_width,
        data_width => vga_data_width
    )
    port map
    (
        clk_a => VGA_CLK_signal,
        addr_in_a => vga_addr_signal_a,
        data_in_a => (others => '0'),
        we_in_a => '0',
        data_out_a => vga_data_signal_a,
        
        clk_b => CLOCK_50,
        addr_in_b => vga_mem_addr,
        data_in_b => vga_mem_data,
        we_in_b => vga_mem_we,
        data_out_b => vga_mem_data_out
    );

end rtl;