-- VGA Core 
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description: 
--  vga core reads data from vga memory and handles the syncing of the VGA protocol
--



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;

entity vga_core is 
    generic 
    (
        -- vga memory 
        addr_width : integer := 17;
        data_width : integer := 3
    );
    port
    (
        -- inputs
        reset_n : in std_logic;
  
        -- memory
        VGA_data_in : in std_logic_vector(data_width-1 downto 0);
        VGA_addr_out : out std_logic_vector(addr_width-1 downto 0);
                
        -- outputs 
        VGA_CLK : in std_logic;
        VGA_VS : out std_logic; 
        VGA_HS : out std_logic;
        VGA_BLANK_N : out std_logic;
        VGA_RGB : out std_Logic_vector(23 downto 0)
    );
end vga_core;
    
architecture rtl of vga_core is 
    -- counter signal 
    type counter_type is 
    record
        x : integer range 0 to 800;
        y : integer range 0 to 525;
    end record;
    signal counter : counter_type;
begin 

-- counters process
process(VGA_CLK, reset_n)
begin
    if reset_n = '0' then
        -- clear counter signals
        counter.x <= 0;
        counter.y <= 0; 
    elsif rising_edge(VGA_CLK) then
        -- increment x every clock 
        if counter.x >= 799 then
            counter.x <= 0; -- start of a new vertical line 
        else
            counter.x <= counter.x + 1; 
        end if;
        
        -- increment y counter when x = 707
        if counter.x = 707 then
            if counter.y >= 525 then 
                counter.y <= 0; -- restart at top of monitor
            else
                counter.y <= counter.y + 1;
            end if;
        end if;

        --if counter.x < 320 and counter.y < 240 then 
        vga_addr_out <= conv_std_logic_vector(((counter.y/2) * 320) + (counter.x/2), addr_width);
        --end if;

    end if;
end process;

-- concurrent statements 

-- sync pulses 
VGA_HS <= '0' when counter.x > 660 and counter.x < 756 else '1';
VGA_VS <= '0' when counter.y = 494 else '1';
VGA_BLANK_N <= '1' when counter.x < 640 and counter.y < 480 else '0';

-- rgb signals
VGA_RGB(3 downto 0) <= (others => vga_data_in(0));
VGA_RGB(7 downto 4) <= (others => vga_data_in(1));

VGA_RGB(11 downto 8) <= (others => vga_data_in(2));
VGA_RGB(15 downto 12) <= (others => vga_data_in(3));

VGA_RGB(19 downto 16) <= (others => vga_data_in(4));
VGA_RGB(23 downto 20) <= (others => vga_data_in(5));

end rtl;