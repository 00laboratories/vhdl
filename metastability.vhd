-- Metastability 
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description:
--  input keys are run through a double d flip flop for metastability 
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity metastability is 
    generic 
    (  
        key_count : integer := 4
    );
    port
    (
        clk : in std_logic;
        reset_n : in std_logic;
        reset_out : out std_logic;
        KEY_in : in std_logic_vector(key_count-1 downto 0);
        KEY_out : out std_logic_vector(key_count-1 downto 0)
    );
end;

architecture rtl of metastability is
    signal key_meta : std_logic_vector(key_count-1 downto 0) := (others => '1');
    signal reset_meta : std_logic := '1';
begin 
    
    process(clk)
    begin
        if rising_edge(clk) then 
            key_meta <= KEY_in;
            KEY_out <= key_meta;
            reset_meta <= reset_n;
            reset_out <= reset_meta;
        end if;
    end process;
end rtl;