 --*****************************************************************************
 -- RAM.vhd
 --
 -- Author: Joakim Lövgren
 --
 -- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
 -- Tool versions: Quartus v16 and ModelSim
 --
 --	Description:
 --		64KB 16 bit RAM
 --*****************************************************************************
 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.global_types.all;

entity RAM is
    port (
        CLOCK : in std_logic;
        reset_n : std_logic;
        we : std_logic;
        addr_in : in std_logic_vector(15 downto 0);
        data_in : in std_logic_vector(15 downto 0);
        data_out : out std_logic_vector(15 downto 0)
    );
end entity RAM;

architecture rtl of RAM is 
    signal RAM_CONTENT : array16(32767 downto 0) := (others => (others => '0'));
begin

    -- main --
    process( CLOCK, reset_n )
        variable addr : integer := 0;
        begin
        if rising_edge( CLOCK ) then
            if reset_n = '0' then 
                data_out <= (others => '0');
                addr := 0;
            else
                -- input addresses are in bytes so divide by 2
                addr := to_integer(unsigned(addr_in)) / 2;
                -- write data
                if we = '1' then 
                    RAM_CONTENT(addr) <= data_in;
                end if;
                -- return data
                data_out <= RAM_CONTENT(addr);
            end if;
        end if;
    end process;
    
end;
