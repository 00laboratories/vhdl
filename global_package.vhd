

library ieee;
use ieee.std_logic_1164.all;

package global_types is
  type array32 is array (natural range <>) of std_logic_vector(31 downto 0);
  type array24 is array (natural range <>) of std_logic_vector(23 downto 0);
  type array16 is array (natural range <>) of std_logic_vector(15 downto 0);
  type array8 is array (natural range <>) of std_logic_vector(7 downto 0);
end package;