library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity segment7 is
    port
    (
        clk         : in  std_logic;
        number      : in  std_logic_vector(3 downto 0); -- 4-bit unsigned integer of 0 to 9.
		segment7    : out std_logic_vector(6 downto 0)  -- 7-segment leds.
    );
end;

architecture segment7_arch of segment7 is

    -- constants for 7 segment display 0 to F
    constant SHOW_ZERO_C: std_logic_vector(6 downto 0) := "1000000";
    constant SHOW_ONE_C: std_logic_vector(6 downto 0) := "1111001";
    constant SHOW_TWO_C: std_logic_vector(6 downto 0) := "0100100";
    constant SHOW_THREE_C: std_logic_vector(6 downto 0) := "0110000";
    constant SHOW_FOUR_C: std_logic_vector(6 downto 0) := "0011001";
    constant SHOW_FIVE_C: std_logic_vector(6 downto 0) := "0010010";
    constant SHOW_SIX_C: std_logic_vector(6 downto 0) := "0000010";
    constant SHOW_SEVEN_C: std_logic_vector(6 downto 0) := "1111000";
    constant SHOW_EIGHT_C: std_logic_vector(6 downto 0) := "0000000";
    constant SHOW_NINE_C: std_logic_vector(6 downto 0) := "0010000";
    constant SHOW_A_C: std_logic_vector(6 downto 0) := "0001000";
    constant SHOW_B_C: std_logic_vector(6 downto 0) := "0000011";
    constant SHOW_C_C: std_logic_vector(6 downto 0) := "1000110";
    constant SHOW_D_C: std_logic_vector(6 downto 0) := "0100001";
    constant SHOW_E_C: std_logic_vector(6 downto 0) := "0000110";
    constant SHOW_F_C: std_logic_vector(6 downto 0) := "0001110";

begin
	 
	process(clk)
	begin
		if rising_edge(clk) then
            case number is 
                when "0000" =>
                    segment7 <= SHOW_ZERO_C;
                when "0001" =>
                    segment7 <= SHOW_ONE_C;
                when "0010" =>
                    segment7 <= SHOW_TWO_C;
                when "0011" =>
                    segment7 <= SHOW_THREE_C;
                when "0100" =>
                    segment7 <= SHOW_FOUR_C;
                when "0101" =>
                    segment7 <= SHOW_FIVE_C;
                when "0110" =>
                    segment7 <= SHOW_SIX_C;
                when "0111" =>
                    segment7 <= SHOW_SEVEN_C;
                when "1000" =>
                    segment7 <= SHOW_EIGHT_C;
                when "1001" =>
                    segment7 <= SHOW_NINE_C;
                when "1010" =>
                    segment7 <= SHOW_A_C;
                when "1011" =>
                    segment7 <= SHOW_B_C;
                when "1100" =>
                    segment7 <= SHOW_C_C;
                when "1101" =>
                    segment7 <= SHOW_D_C;
                when "1110" =>
                    segment7 <= SHOW_E_C;
                when "1111" =>
                    segment7 <= SHOW_F_C;
                when others =>
                    segment7 <= show_E_C; -- error
            end case;
        end if;
	end process;
    
end architecture;