--*****************************************************************************
-- LED_OUT.vhd
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
--	Description:
--		led output component    
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LED_OUT is
    port (
        CLOCK : in std_logic;
        reset_n : in std_logic;
        data_in : in std_logic_vector(15 downto 0);
        data_out : out std_logic_vector(15 downto 0)
    );
end entity LED_OUT;

architecture rtl of LED_OUT is
    signal LED_VALUES : std_logic_vector(15 downto 0) := (others => '0');
begin

    data_out(0) <= '1' when data_in(0) = '1' else '0';
    data_out(1) <= '1' when data_in(1) = '1' else '0';
    data_out(2) <= '1' when data_in(2) = '1' else '0';
    data_out(3) <= '1' when data_in(3) = '1' else '0';
    data_out(4) <= '1' when data_in(4) = '1' else '0';
    data_out(5) <= '1' when data_in(5) = '1' else '0';
    data_out(6) <= '1' when data_in(6) = '1' else '0';
    data_out(7) <= '1' when data_in(7) = '1' else '0';
    data_out(8) <= '1' when data_in(8) = '1' else '0';
    data_out(9) <= '1' when data_in(9) = '1' else '0';
    data_out(15 downto 10) <= (others => '0');

    -- LED_OUT --
    /*process( CLOCK, reset_n )
        begin
        if reset_n = '0' then 
            LED_VALUES <= (others => '0');
            data_out <= (others => '0');
        elsif rising_edge( CLOCK ) then
            LED_VALUES <= data_in;
            data_out <= LED_VALUES;
        end if;
    end process;-*/
    
end;