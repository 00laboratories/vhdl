--*****************************************************************************
-- CPU.vhd
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
--	Description:
--		16 bit cpu that controls most of the construction
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CPU is
    port (
        CLOCK_50 : in std_logic;
        reset_n  : in std_logic;
        -- communication with bus 
        bus_port_select : out integer range 0 to 8;
        bus_data_out    : out std_logic_vector(15 downto 0);
        bus_addr_out    : out std_logic_vector(15 downto 0);
        bus_we_out      : out std_logic;
        bus_data_in     : in std_logic_vector(15 downto 0);
        -- vJTAG fifo data 
        fifo_data       : in std_logic_vector(15 downto 0);
        fifo_full       : in std_logic;
        fifo_empty      : in std_logic;
        fifo_clock      : out std_logic;
        fifo_read_enable: out std_logic
    );
end entity CPU;

architecture rtl of CPU is
    -- cpu registers:
    -- 0-7 = generic registers 
    -- 8 = stack pointer 
    -- 9 = status flags bits ->
    -- Bit # | -- Abbreviation -- Description ---
    -- |  0  |        ZF        |   Zero Flag   | 
    -- |  1  |        SF        |   Sign Flag   | 
    ---------------------------------------------
    -- 10 program counter
    type register_type is array (integer range <>) of std_logic_vector(15 downto 0); 
    -- cpu states
    type cpu_states is (
        fifo_read_state, fifo_read_state_2, fifo_read_state_3,
        fifo_decode,
        reading_rom_state, reading_rom_state2, reading_rom_state3, reading_rom_state4,
        error_state, fetch_state, fetch_state2, 
        decode_state, 
        draw_pixel_state, draw_pixel_state_2, draw_pixel_state_3, draw_pixel_state_4, draw_pixel_state_5, 
        push_state, push_state_2,
        pop_state, pop_state_2, pop_state_3, pop_state_4,
        mov_state, mov_state_2, mov_state_3, mov_state_4, mov_state_5, mov_state_6,
        cmp_state, cmp_state_2, cmp_state_3, cmp_state_4,
        call_state, call_state_2,
        ret_state, ret_state_2,
        jmp_state, jmp_state_2,
        arithmetic_state, arithmetic_state_2,arithmetic_state_3, arithmetic_state_4, arithmetic_state_5,
        bitwise_state, bitwise_state_2,bitwise_state_3, bitwise_state_4,
        logic_state, logic_state_2,logic_state_3, logic_state_4,
        bus_com_state, bus_com_state_2, bus_com_state_3, bus_com_state_4, bus_com_state_5, bus_com_state_6
        );
    signal state : cpu_states := reading_rom_state;

    type fifo_states is (
        fifo_reading_state,
        fifo_updating_ram_state_1, fifo_updating_ram_state_2, 
        fifo_error_state
    );
    signal fifo_state : fifo_states := fifo_reading_state;

    --signal bus_data_in : std_logic_vector(15 downto 0) := (others => '0');
    signal bus_data_in_2 : std_logic_vector(15 downto 0) := (others => '0');

    signal CLOCK_25 : std_logic := '0';
begin
    
    fifo_clock <= CLOCK_25;

    -- clock divider --
    process( CLOCK_50 )
        begin
        if rising_edge( CLOCK_50 ) then
            CLOCK_25 <= not CLOCK_25;
        end if;
    end process;
    
    -- Core loop --
    process(CLOCK_25, reset_n)
        -- cpu registers:
        -- 0-7 = generic registers 
        -- 8 = stack pointer 
        -- 9 = status flags bits ->
        -- Bit # | -- Abbreviation -- Description ---
        -- |  0  |        ZF        |   Zero Flag   | 
        -- |  1  |        SF        |   Sign Flag   | 
        ---------------------------------------------
        -- 10 program counter
        variable cpu_registers : register_type(10 downto 0);
        variable opcode : std_logic_vector(15 downto 0) := (others => '0');
        variable rom_counter : integer := 0;
        variable target_reg : integer := 0;
        variable source_reg : integer := 0;
        variable math_32 : unsigned(31 downto 0) := (others => '0'); -- used for temporary math results
        
        -- fifo 
        variable reading_fifo    : boolean := false;
        variable fifo_bytes_read : integer range 0 to 7 := 0;
        variable fifo_word       : std_logic_vector(15 downto 0) := (others => '0');
        variable fifo_opcode     : std_logic_vector(15 downto 0) := (others => '0');

        variable update_ram_length : unsigned(15 downto 0) := (others => '0');
        variable update_ram_counter : unsigned(15 downto 0) := (others => '0');

        -- DRAWING
        variable draw_read_counter : integer := 0;

        begin
        if reset_n = '0' then 
            cpu_registers := (others => (others => '0'));
             -- stack pointer starts at end of ram which is 32767 Words long
            cpu_registers(8) := X"7FFF";
            opcode := (others => '0');
            rom_counter := 0;
            target_reg := 0;
            source_reg := 0;
            math_32 := (others => '0');
            bus_port_select <= 0;
            bus_data_out <= (others => '0');
            bus_addr_out <= (others => '0');
            bus_we_out <= '0';
            reading_fifo := false;
            state <= reading_rom_state;
            fifo_state <= fifo_reading_state;
            draw_read_counter := 0;
        elsif rising_edge( CLOCK_25 ) then
            bus_data_in_2 <= bus_data_in; -- metastability for slower nets
            case state is 
                -- clock fifo 
                when fifo_read_state =>
                    reading_fifo := true;
                    -- if there is data in the stack 
                    if fifo_empty = '0' then 
                        state <= fifo_read_state_2;
                        fifo_read_enable <= '1';
                    end if;
                when fifo_read_state_2 =>
                    state <= fifo_read_state_3;
                when fifo_read_state_3 =>                    
                    -- jtag is only sending bytes 
                    fifo_word(7 downto 0) := fifo_data(7 downto 0);
                    -- rotate vector before reading next byte 
                    fifo_word := std_logic_vector(unsigned(fifo_word) ror 8);
                    
                    -- decode when a full word has been read 
                    if fifo_bytes_read = 1 then 
                        state <= fifo_decode;
                        fifo_bytes_read := 0;
                    else 
                        state <= fifo_read_state; 
                        fifo_bytes_read := fifo_bytes_read + 1;
                    end if;
    
                    fifo_read_enable <= '0';

                when fifo_decode =>
                    -- read more data from fifo 
                    state <= fifo_read_state;

                    case fifo_state is 
                        when fifo_reading_state =>
                            -- update ram state 
                            if fifo_word = X"0001" then 
                                fifo_state <= fifo_updating_ram_state_1;
                            -- unknown opcode
                            else 
                                fifo_state <= fifo_error_state;
                            end if;
                        -- update state 1 reads the length of data to come 
                        when fifo_updating_ram_state_1 =>
                            update_ram_counter := to_unsigned(0,16);
                            update_ram_length := unsigned(fifo_word); -- count is in bytes so divide to get words
                            fifo_state <= fifo_updating_ram_state_2;  
                            bus_port_select <= 1; -- RAM
                            bus_we_out <= '1'; -- write enable 
                        -- update state 2 writes to ram 
                        when fifo_updating_ram_state_2 =>
                                bus_data_out <= fifo_word;
                                bus_addr_out <= std_logic_vector(update_ram_counter); 
                            -- write to ram      
                            if update_ram_counter < (update_ram_length-2) then  
                                update_ram_counter := update_ram_counter + 2;  -- ram takes byte index
                            else 
                                -- CPU state back to fetch and run on new ram content 
                                state <= fetch_state;
                                -- reset cpu registers 
                                cpu_registers := (others => (others => '0'));
                                cpu_registers(8) := X"FFFF";
                                
                                reading_fifo := false; -- should be no more content to read in fifo 
                                fifo_state <= fifo_reading_state;
                            end if;
                        when fifo_error_state =>
                            bus_data_out <= X"EFFE"; -- error data to 7 seg display 
                            bus_port_select <= 3;
                    end case;
                                   
               -- read rom contents
                when reading_rom_state =>   
                    -- enable read for ram
                    bus_we_out <= '1'; 
                    bus_port_select <= 2; -- read from ROM port 
                    -- rom counter keeps track of read and write addr
                    bus_addr_out <= std_logic_vector(to_unsigned(rom_counter,16)); 
                    state <= reading_rom_state2;
                -- intermediate stage to get data from rom
                when reading_rom_state2 =>
                    state <= reading_rom_state3;
                when reading_rom_state3 => 
                    -- write data to ram 
                    bus_data_out <= bus_data_in; 
                    bus_port_select <= 1; -- change to RAM port
                    -- ram uses byte addressing so * 2
                    bus_addr_out <= std_logic_vector(to_unsigned(rom_counter * 2,16)); 
                    state <= reading_rom_state4;
                -- intermediate stage to write to ram 
                when reading_rom_state4 =>
                    -- increment counter 
                    rom_counter := rom_counter + 1;
                    -- everything has been written to ram
                    if rom_counter = 268 / 2 then 
                        state <= fetch_state; 
                        bus_we_out <= '0';
                    else 
                        state <= reading_rom_state;      
                    end if;
                when fetch_state =>
                    -- set bus to RAM port 
                    bus_port_select <= 1;
                    bus_we_out <= '0';
                    -- set bus addr to program counter 
                    bus_addr_out <= cpu_registers(10); -- PC
                    -- intermediate stage to give ram time to return data
                    state <= fetch_state2;
                when fetch_state2 =>
                    -- go to decode state
                    state <= decode_state;
                when decode_state =>
                    -- increment PC 
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- store opcode 
                    opcode := bus_data_in;

                    -----------------------------------------
                    ------           decode           -------
                    -----------------------------------------

                    bus_addr_out <= cpu_registers(10); -- read next address
                    
                    -- PUSH 
                   if opcode = X"0017" or opcode = X"0018" then -- push 
                        state <= push_state;
                    -- POP 
                    elsif opcode = X"0019" then 
                        state <= pop_state;
                    -- MOV 
                    elsif unsigned(opcode) >= X"0001" and unsigned(opcode) <= X"0008" then 
                        state <= mov_state;
                    -- JMP 
                   elsif (unsigned(opcode) >= X"0009" and unsigned(opcode) <= X"000c") or (unsigned(opcode) >= X"0037" and unsigned(opcode) <= X"003c") then 
                        state <= jmp_state;
                    -- ARITHMETIC add, sub, mul, div
                    elsif unsigned(opcode) >= X"000d" and unsigned(opcode) <= X"0014" then 
                        state <= arithmetic_state;
                   -- BITWISE and, or, nand, nor, xor, xnor, not 
                    elsif unsigned(opcode) >= X"001e" and unsigned(opcode) <= X"002a" then 
                        state <= bitwise_state;
                    -- LOGIC sll, srl, rol, ror 
                    elsif unsigned(opcode) >= X"002b" and unsigned(opcode) <= X"0036" then 
                        state <= logic_state;
                    -- CMP 
                    elsif opcode = X"0015" or  opcode = X"0016" then
                        state <= cmp_state;
                    -- CALL 
                    elsif opcode = X"001a" or opcode = X"001b" then 
                        state <= call_state;
                    -- RET 
                    elsif opcode = X"001c" then 
                        state <= ret_state;
                        bus_addr_out <= std_logic_vector(unsigned(cpu_registers(8))+1); -- read return value from stack
                    -- BUS COMMUNICATION - IN and OUT
                    elsif unsigned(opcode) >= X"003d" and unsigned(opcode) <= X"003f" then 
                        state <= bus_com_state;
                    -- DRAW PIXEL
                    elsif opcode = X"001d" then 
                        state <= draw_pixel_state;
                    else 
                        state <= error_state;
                        assert false
                        --report "ERROR: unknown opcode: '" & to_hstring(opcode) & "'" &
                        --"PC: " & to_hstring(cpu_registers(10))
                        severity failure;
                    end if;
                    -- intermediate stage to get data from ram 
                when error_state =>
                    bus_port_select <= 3;
                    bus_data_out <= opcode; -- display opcode value when error
                -- PUSH 
                when push_state => 
                    state <= push_state_2;
                    -- state 2 has the data from the opcode 
                when push_state_2 => 
                    -- write data to stack
                    bus_we_out <= '1';
                    bus_addr_out <= cpu_registers(8); -- SP 
                    -- determine if write register value or imm value 
                    if opcode = X"0017" then -- imm 
                        bus_data_out <= bus_data_in; 
                    elsif opcode = X"0018" then -- register 
                        -- get register integer 
                        target_reg := to_integer(unsigned(bus_data_in));
                        -- write value of register to stack 
                        bus_data_out <= cpu_registers(target_reg);
                    end if; 
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- decrement stack pointer 
                    cpu_registers(8) := std_logic_vector(unsigned(cpu_registers(8)) - 2);
                    -- back to fetch state 
                    state <= fetch_state;
                -- POP 
                when pop_state =>
                    state <= pop_state_2;
                when pop_state_2 =>
                    -- target register for stack value 
                    target_reg := to_integer(unsigned(bus_data_in));         
                    -- read value on top of stack 
                    bus_addr_out <= std_logic_vector(unsigned(cpu_registers(8)) + 2);   
                    state <= pop_state_3;
                when pop_state_3 => -- reading stack from RAM
                    state <= pop_state_4;
                when pop_state_4 => 
                    -- write stack value to register and increment stack 
                    cpu_registers(target_reg) := bus_data_in;
                    cpu_registers(8) := std_logic_vector(unsigned(cpu_registers(8)) + 2);
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    state <= fetch_state;
                -- MOV
                when mov_state =>
                    state <= mov_state_2;
                when mov_state_2 =>
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                    -- if writing to reg, imm
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    --if opcode = X"0004" or opcode = X"0006" then 
                    --    target_reg := to_integer(unsigned(bus_data_in));    
                    --    target_reg := to_integer(unsigned(cpu_registers(target_reg))); -- target write addr
                    --end if;
                    state <= mov_state_3;
                when mov_state_3 => -- some time to read ram 
                    state <= mov_state_4;
                when mov_state_4 =>
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2); -- increment PC
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                    state <= fetch_state;  
                    -- reg, imm
                    if opcode = X"0001" then 
                        cpu_registers(target_reg) := bus_data_in;
                     -- reg, reg     
                    elsif opcode = X"0002" then 
                        source_reg := to_integer(unsigned(bus_data_in));
                        cpu_registers(target_reg) := cpu_registers(source_reg);       
                    -- [imm], imm
                    elsif opcode = X"0003" then 
                        bus_we_out <= '1'; -- write to [imm] addr 
                        bus_addr_out <= std_logic_vector(to_unsigned(target_reg,16)); 
                        bus_data_out <= bus_data_in;
                    -- [reg], imm
                    elsif opcode = X"0004" then 
                        bus_we_out <= '1'; -- write to [reg] addr 
                        bus_addr_out <= cpu_registers(target_reg);
                        bus_data_out <= bus_data_in;
                    -- [imm], reg 
                    elsif opcode = X"0005" then
                        source_reg := to_integer(unsigned(bus_data_in));
                        bus_we_out <= '1'; -- write to [imm] addr 
                        bus_addr_out <= std_logic_vector(to_unsigned(target_reg,16)); 
                        bus_data_out <= cpu_registers(source_reg);
                    -- [reg], reg
                    elsif opcode = X"0006" then 
                        source_reg := to_integer(unsigned(bus_data_in));
                        bus_we_out <= '1'; -- write to [reg] addr 
                        bus_addr_out <= cpu_registers(target_reg);
                        bus_data_out <= cpu_registers(source_reg);
                    -- reg, [imm]
                    elsif opcode = X"0007" then 
                        bus_addr_out <= bus_data_in;
                        state <= mov_state_5; -- extra state needed to read from memory
                    -- reg, [reg]
                    elsif opcode = X"0008" then 
                        source_reg := to_integer(unsigned(bus_data_in));
                        bus_addr_out <= cpu_registers(source_reg);
                        state <= mov_state_5;  -- extra state needed to read from memory
                    end if;  
                when mov_state_5 =>  
                    state <= mov_state_6;      
                when mov_state_6 =>     
                    -- reg, [mem]
                    cpu_registers(target_reg) := bus_data_in;
                    state <= fetch_state;   
                -- JMP instructions
                when jmp_state =>
                    state <= jmp_state_2;
                when jmp_state_2 =>
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    -- jmp imm
                    if opcode = X"0009" then
                        cpu_registers(10) := bus_data_in;-- set PC to new addr  
                    -- jmp reg 
                    elsif opcode = X"000a" then 
                        cpu_registers(10) := cpu_registers(target_reg);-- set PC to new addr    
                    -- je imm 
                    elsif opcode = X"000b" and cpu_registers(9)(0) = '1' then 
                        cpu_registers(10) := bus_data_in;-- set PC to new addr  
                    -- je reg 
                    elsif opcode = X"000c" and cpu_registers(9)(0) = '1'  then 
                        cpu_registers(10) := cpu_registers(target_reg);-- set PC to new addr  
                    -- jne imm
                    elsif opcode = X"0037" and cpu_registers(9)(0) = '0'  then
                        cpu_registers(10) := bus_data_in;-- set PC to new addr     
                    -- jne reg
                    elsif opcode = X"0038" and cpu_registers(9)(0) = '0'  then
                        cpu_registers(10) := cpu_registers(target_reg);-- set PC to new addr  
                    -- jg imm
                    elsif opcode = X"0039" and cpu_registers(9)(0) = '0' and cpu_registers(9)(1) = '1' then
                        cpu_registers(10) := bus_data_in;-- set PC to new addr 
                    -- jg reg
                    elsif opcode = X"003a" and cpu_registers(9)(0) = '0' and cpu_registers(9)(1) = '1' then
                        cpu_registers(10) := cpu_registers(target_reg);-- set PC to new addr  
                    -- jl imm
                    elsif opcode = X"003b" and cpu_registers(9)(0) = '0' and cpu_registers(9)(1) = '0'  then 
                        cpu_registers(10) := bus_data_in;-- set PC to new addr 
                    -- jl reg
                    elsif opcode = X"003c" and cpu_registers(9)(0) = '0' and cpu_registers(9)(1) = '0'  then 
                        cpu_registers(10) := cpu_registers(target_reg);-- set PC to new addr  
                    -- jump not taken
                    else 
                        cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    end if;
                state <= fetch_state;
                -- ARITHMETIC  
                when arithmetic_state =>
                    state <= arithmetic_state_2;
                when arithmetic_state_2 =>
                    -- first argument is always a destination register
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    state <= arithmetic_state_3;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                when arithmetic_state_3 =>
                    -- division uses delayed data input
                    if opcode = X"0013" or opcode = X"0014" then 
                        target_reg := to_integer(unsigned(bus_data_in_2)); 
                    end if;
                    state <= arithmetic_state_4;
                when arithmetic_state_4 =>
                    -- get source register
                    source_reg := to_integer(unsigned(bus_data_in)); 
                    
                    state <= fetch_state;
                    
                    if opcode = X"000d" then -- add reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) + to_unsigned(source_reg,16));
                    elsif opcode = X"000e" then -- add reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) + unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"000f" then -- sub reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) - to_unsigned(source_reg,16));
                    elsif opcode = X"0010" then -- sub reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) - unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0011" then -- mul reg, imm
                        -- extend left hand side for the multiplication to work
                        math_32 := unsigned(cpu_registers(target_reg)) * to_unsigned(source_reg,16);
                        cpu_registers(target_reg) := std_logic_vector(math_32(15 downto 0));
                    elsif opcode = X"0012" then -- mul reg, reg
                        math_32 := unsigned(cpu_registers(target_reg)) * unsigned(cpu_registers(source_reg));
                        cpu_registers(target_reg) := std_logic_vector(math_32(15 downto 0));
                    elsif opcode = X"0013" or opcode = X"0014" then -- division
                       state <= arithmetic_state_5;
                    end if;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);   
                when arithmetic_state_5 =>
                    source_reg := to_integer(unsigned(bus_data_in_2)); 
                    if opcode = X"0013" then -- div reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) / to_unsigned(source_reg,16));
                    elsif opcode = X"0014" then -- div reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) / unsigned(cpu_registers(source_reg)));
                    end if;
                    state <= fetch_state;
                -- BITWISE  
                when bitwise_state =>
                    state <= bitwise_state_2;
                when bitwise_state_2 =>
                    -- first argument is always a destination register
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    state <= bitwise_state_3;
                    -- not reg
                    if opcode = X"002a" then 
                        cpu_registers(target_reg) := std_logic_vector(not unsigned(cpu_registers(target_reg)));
                        state <= fetch_state; -- not only uses 1 argument so it can return to fetch from here
                    end if;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                when bitwise_state_3 =>
                    state <= bitwise_state_4;
                when bitwise_state_4 =>
                    -- get source register
                    source_reg := to_integer(unsigned(bus_data_in)); 
                
                    if opcode = X"001e" then -- and reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) and to_unsigned(source_reg,16));
                    elsif opcode = X"001f" then -- and reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) and unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0020" then -- or reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) or to_unsigned(source_reg,16));
                    elsif opcode = X"0021" then -- or reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) or unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0022" then -- nand reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) nand to_unsigned(source_reg,16));
                    elsif opcode = X"0023" then -- nand reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) nand unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0024" then -- nor reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) nor to_unsigned(source_reg,16));
                    elsif opcode = X"0025" then -- nor reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) nor unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0026" then -- xor reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) xor to_unsigned(source_reg,16));
                    elsif opcode = X"0027" then -- xor reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) xor unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0028" then -- xnor reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) xnor to_unsigned(source_reg,16));
                    elsif opcode = X"0029" then -- xnor reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) xnor unsigned(cpu_registers(source_reg)));
                    end if;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    state <= fetch_state;
                -- LOGIC  
                when logic_state =>
                    state <= logic_state_2;
                when logic_state_2 =>
                    -- first argument is always a destination register
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    state <= logic_state_3;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                when logic_state_3 =>
                    state <= logic_state_4;
                when logic_state_4 =>
                    -- get source register
                    source_reg := to_integer(unsigned(bus_data_in)); 
                    if opcode = X"002b" then -- sll reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) sll source_reg);
                    elsif opcode = X"002c" then -- sll reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) sll to_integer(unsigned(cpu_registers(source_reg))));
                    elsif opcode = X"002d" then -- srl reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) srl source_reg);
                    elsif opcode = X"002e" then -- srl reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) srl to_integer(unsigned(cpu_registers(source_reg))));
                    elsif opcode = X"002f" then -- sla reg, imm
                    --     cpu_registers(target_reg) := cpu_registers(target_reg) sla to_unsigned(source_reg,16);
                    -- elsif opcode = X"0030" then -- sla reg, reg
                    --     cpu_registers(target_reg) := cpu_registers(target_reg) sla to_integer(unsigned(cpu_registers(source_reg)));
                    -- elsif opcode = X"0031" then -- sra reg, imm
                    --     cpu_registers(target_reg) := cpu_registers(target_reg) sra source_reg;
                    -- elsif opcode = X"0032" then -- sra reg, reg
                    --     cpu_registers(target_reg) := cpu_registers(target_reg) sra to_integer(unsigned(cpu_registers(source_reg)));
                    elsif opcode = X"0033" then -- rol reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) rol source_reg);
                    elsif opcode = X"0034" then -- rol reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) rol to_integer(unsigned(cpu_registers(source_reg))));
                    elsif opcode = X"0035" then -- ror reg, imm
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) ror source_reg);
                    elsif opcode = X"0036" then -- ror reg, reg
                        cpu_registers(target_reg) := std_logic_vector(unsigned(cpu_registers(target_reg)) ror to_integer(unsigned(cpu_registers(source_reg))));
                    end if;
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    state <= fetch_state;
                -- CMP 
                when cmp_state =>
                    state <= cmp_state_2;
                when cmp_state_2 =>
                    -- first argument is the register being compared
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                    state <= cmp_state_3;
                when cmp_state_3 =>
                    state <= cmp_state_4;
                when cmp_state_4 =>
                    -- set flags to 0
                    cpu_registers(9)(0) := '0'; -- ZF
                   
                    -- cmp reg, imm
                    if opcode = X"0015" then 
                        math_32(15 downto 0) := unsigned(cpu_registers(target_reg)) - unsigned(bus_data_in);
                    -- cmp reg, reg 
                    elsif opcode = X"0016" then 
                        -- get source register
                        source_reg := to_integer(unsigned(bus_data_in)); 
                        math_32(15 downto 0) := unsigned(cpu_registers(target_reg)) - unsigned(cpu_registers(source_reg)); 
                    end if;

                    cpu_registers(9)(1) := math_32(15); -- set sign flag
                    
                    -- check results
                    if math_32 = 0 then 
                        cpu_registers(9)(0) := '1'; -- set ZF flag to 1
                    end if;

                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    state <= fetch_state;
                -- CALL
                when call_state =>
                    state <= call_state_2;
                when call_state_2 =>
                    -- write next PC to stack for the RET opcode
                    bus_we_out <= '1';
                    bus_data_out <= std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- write to stack addr
                    bus_addr_out <= std_logic_vector(unsigned(cpu_registers(8)));
                    -- call imm
                    if opcode = X"001a"then 
                        cpu_registers(10) := std_logic_vector(unsigned(bus_data_in)); -- set PC from imm
                    -- call reg
                    elsif opcode = X"001b" then
                        target_reg := to_integer(unsigned(bus_data_in));
                        cpu_registers(10) := cpu_registers(target_reg);--set PC from register
                    end if;
                    -- decrement stack 
                    cpu_registers(8) := std_logic_vector(unsigned(cpu_registers(8)) - 2);
                    state <= fetch_state;
                -- RET
                when ret_state =>
                    state <= ret_state_2;
                when ret_state_2 =>
                    cpu_registers(10) :=  bus_data_in;-- set PC to the return addr
                    -- increment stack 
                    cpu_registers(8) := std_logic_vector(unsigned(cpu_registers(8)) + 2);
                    state <= fetch_state;
                -- BUS COMMUNICATION - IN and OUT
                when bus_com_state =>
                    state <= bus_com_state_2;
                when bus_com_state_2 =>
                    -- store the port to read or write from 
                    target_reg := to_integer(unsigned(bus_data_in)); 
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- read next value 
                    bus_addr_out <= cpu_registers(10);
                    
                    state <= bus_com_state_3;
                when bus_com_state_3 =>
                    state <= bus_com_state_4;
                when bus_com_state_4 => 
                    -- increment PC
                    cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2);
                    -- source register
                    source_reg := to_integer(unsigned(bus_data_in)); 
                    -- set bus port for read / write
                    bus_port_select <= target_reg;
                    bus_addr_out <= cpu_registers(7); -- r8 is used for bus addr
                   
                    state <= fetch_state;
                    -- read from port into register
                    if opcode = X"003d" then 
                        state <= bus_com_state_5;
                    -- write data from register to port 
                    elsif opcode = X"003e" then -- output imm 
                        bus_we_out <= '1';
                        bus_data_out <= bus_data_in;
                    elsif opcode = X"003f" then -- output register value
                        bus_data_out <= cpu_registers(source_reg);
                        bus_we_out <= '1'; 
                    end if;

                
                when bus_com_state_5 =>
                    state <= bus_com_state_6;
                -- get input from port 
                when bus_com_state_6 =>
                    cpu_registers(source_reg) := bus_data_in;
                    state <= fetch_state;
                when draw_pixel_state =>
                    -- read data from ram                          
                    bus_port_select <= 1;
                    bus_we_out <= '0';

                    state <= draw_pixel_state_2;
                when draw_pixel_state_2 =>  
                    state <= draw_pixel_state_3; 
                when draw_pixel_state_3 =>                 
                    target_reg := to_integer(unsigned(bus_data_in));

                    state <= draw_pixel_state_4;
                    bus_port_select <= 0; -- select GPU
                    bus_we_out <= '1';
                    if opcode = X"001d" then 
                        -- send opcode
                        if draw_read_counter = 0 then 
                            bus_data_out <= opcode;
                        -- read x
                        elsif draw_read_counter = 1 then     
                            bus_data_out <= cpu_registers(target_reg);
                            cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2); 
                        -- read y 
                        elsif draw_read_counter = 2 then 
                            bus_data_out <= cpu_registers(target_reg);
                            cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2); 
                        -- read color 
                        elsif draw_read_counter = 3 then
                            bus_data_out <= cpu_registers(target_reg);
                            cpu_registers(10) := std_logic_vector(unsigned(cpu_registers(10)) + 2); 
                        end if;
                    end if;

                    draw_read_counter := draw_read_counter + 1;
                    -- return to fetch
                    if draw_read_counter > 4 then 
                        state <= fetch_state;
                        draw_read_counter := 0;
                        bus_we_out <= '0';
                    end if;
                    
                    bus_addr_out <= cpu_registers(10);

                when draw_pixel_state_4 =>  
                    bus_we_out <= '0';
                    state <= draw_pixel_state_5;
                when draw_pixel_state_5 =>
                    state <= draw_pixel_state;
            end case;

            -- read vJTAG data from fifo stack 
            if fifo_empty = '0' and reading_fifo = false then
                state <= fifo_read_state;
                bus_port_select <= 3;
                bus_data_out <= X"8888";
            end if;  

        end if;
    end process;
end;

