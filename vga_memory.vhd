-- VGA memory 
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description:
--  video memory for the VGA component
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
    
entity vga_memory is 
    generic (
        size : integer := 320*240;
        addr_width : integer := 17;
        data_width : integer := 3
    );
    port (
        clk_a : in std_logic;
        clk_b : in std_logic;
        -- inputs
        addr_in_a : in std_logic_vector(addr_width-1 downto 0);
        data_in_a : std_logic_vector(data_width-1 downto 0);
        we_in_a : in std_logic; -- write enable
         -- inputs
        addr_in_b : in std_logic_vector(addr_width-1 downto 0);
        data_in_b : std_logic_vector(data_width-1 downto 0);
        we_in_b : in std_logic; -- write enable
        -- outputs
        data_out_a : out std_logic_vector(data_width-1 downto 0);
        data_out_b : out std_logic_vector(data_width-1 downto 0)
    );
    
end;

architecture rtl of vga_memory is
    type mem_type is array(integer range <>) of std_logic_vector(data_width-1 downto 0);
    shared variable memory : mem_type(size-1 downto 0) := (others => (others => '0'));
begin  
    
    -- process A
    process(clk_a)
        begin 
        if rising_edge(clk_a) then 
            -- write data to ram 
            if we_in_a = '1' then 
                memory(conv_integer(addr_in_a)) := data_in_a;
            end if;
            -- output data 
            data_out_a <= memory(conv_integer(addr_in_a));
        end if;
    end process;
    
    -- process B
    process(clk_b)
        begin 
        if rising_edge(clk_b) then 
            -- write data to ram 
            if we_in_b = '1' then 
               memory(conv_integer(addr_in_b)) := data_in_b;
            end if;
        
            -- output data 
            data_out_b <= memory(conv_integer(addr_in_b));
        end if;
    end process;
end;