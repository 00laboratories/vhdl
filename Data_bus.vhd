
--*****************************************************************************
-- Data_bus.vhd 
--
-- Author: Jocke
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
-- Description:
--  8 port 16 bit communication bus for data between cpu and other devices
--  
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.global_types.all;

entity Data_bus is
    generic 
    (
        bus_width : integer := 16;
        bus_ports : integer := 8
    );
    port (
        -- inputs
        reset_n     : in std_logic;
        CLOCK       : in std_logic;
        port_select : in integer range 0 to bus_ports;
        devices_data_in : in array16(bus_ports-1 downto 0);
        addr_in     : in std_logic_vector(bus_width-1 downto 0);
        data_in     : in std_logic_vector(bus_width-1 downto 0);
        we_in       : in std_logic; -- write enable from cpu
        -- outputs
        devices_addr_out : out array16(bus_ports-1 downto 0);
        devices_data_out : out array16(bus_ports-1 downto 0);
        devices_we_out   : out std_logic_vector(bus_ports-1 downto 0);
        data_out    : out std_logic_vector(bus_width-1 downto 0)
    );
end entity Data_bus;

architecture str of Data_bus is
begin

    process(reset_n, CLOCK)
    begin
        if reset_n = '0' then
            devices_addr_out <= (others => (others => '0'));
            devices_data_out <= (others => (others => '0'));
            devices_we_out <= (others => '0');
            data_out <= (others => '0');
        elsif rising_edge(CLOCK) then
            devices_addr_out(port_select) <= addr_in; 
            devices_data_out(port_select) <= data_in;
            data_out <= devices_data_in(port_select);
            devices_we_out(port_select) <= we_in;
        end if;
    end process;

end architecture str;