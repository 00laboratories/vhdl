--*****************************************************************************
-- GPU.vhd
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
--	Description:
--		handles the communication between vga component and cpu
--      provides commands for drawing
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GPU is
    port (
        CLOCK_50 : in std_logic;
        CLOCK_CPU : in std_logic; -- clock from cpu for the fifo
        reset_n  : in std_logic;

        -- connect to bus
        bus_addr_in  : in std_logic_vector(15 downto 0);
        bus_data_in  : in std_logic_vector(15 downto 0);
        bus_we       : in std_logic;  -- indicates when the cpu is sending instructions to gpu
        bus_data_out : out std_logic_vector(15 downto 0);

        -- ports for reading and writing to vga memory 
        vga_addr_out : out std_logic_vector(16 downto 0);
        vga_data_out : out std_logic_vector(5 downto 0);
        vga_we       : out std_logic;
        vga_data_in  : in std_logic_vector(5 downto 0)
    );
end entity GPU;

architecture rtl of GPU is
    
    component altera_fifo IS
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q		    : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
    end component altera_fifo;

    -- fifo stack for gpu commands
    type gpu_commands is array(integer range <>) of std_logic_vector(15 downto 0);
    -- gpu states 
    type gpu_states is (
        reading_data_state, reading_data_state_2, reading_data_state_3, 
        drawing_state
        );
    
    signal state : gpu_states := reading_data_state;

    -- FIFO signals 
    signal fifo_read_en : std_logic := '0';
    signal fifo_data : std_logic_vector(15 downto 0) := (others => '0');
    signal fifo_empty : std_logic := '1';
    signal fifo_full : std_logic := '0';
    
    signal fifo_reset : std_logic := '0';

begin

    gpu_fifo_inst : altera_fifo
    port map(
        fifo_reset,
        bus_data_in,
        CLOCK_50,
        fifo_read_en,
        CLOCK_CPU,
        bus_we,
        fifo_data,
        fifo_empty,
        fifo_full
    );
    fifo_reset <= not reset_n;
    
    bus_data_out <= "000000000000000" & fifo_full;

    -- Core --
    process( CLOCK_50, reset_n )
        variable command_stack : gpu_commands(0 to 16) := (others => (others => '0'));
        variable command_pointer : integer range 0 to 16 := 0;
        -- keeps track of where we are in the stack while drawing
        variable draw_counter : integer range 0 to 16 := 0;

        -- command data 
        variable command : std_logic_vector(15 downto 0) := (others => '0');
        variable x_pos : integer := 0;
        variable y_pos : integer := 0;
        variable rgb_color : std_logic_vector(5 downto 0) := (others => '0');

        -- fifo 
        variable fifo_words_read : integer := 0;

        begin
        -- zero everything
        if reset_n = '0' then 
            vga_we <= '0';
            vga_data_out <= (others => '0');
            vga_addr_out <= (others => '0');
            state <= reading_data_state;
            fifo_read_en <= '0';
            fifo_words_read := 0;
            draw_counter := 0;
            command_pointer := 0;
        elsif rising_edge( CLOCK_50 ) then
            case state is 
                when drawing_state =>
                    -- execute draw commands
                    
                    -- write to vga memory
                    vga_we <= '1';
                    -- get command from stack
                    command := command_stack(draw_counter);
                    
                    -- DRAW PIXEL
                    if command = X"001d" then 
                        -- get arguments
                        x_pos := to_integer(unsigned(command_stack(draw_counter+1)) mod 320);
                        y_pos := to_integer(unsigned(command_stack(draw_counter+2)) mod 240);

                        rgb_color := command_stack(draw_counter+3)(5 downto 0);
                        -- set pixel address
                        vga_addr_out <= std_logic_vector(to_unsigned(x_pos + (320*y_pos), 17));
                        -- set pixel color 
                        vga_data_out(0) <= rgb_color(0);
                        vga_data_out(1) <= rgb_color(1);

                        vga_data_out(2) <= rgb_color(2);
                        vga_data_out(3) <= rgb_color(3);

                        vga_data_out(4) <= rgb_color(4);
                        vga_data_out(5) <= rgb_color(5);

                        draw_counter := draw_counter + 4; -- go to next command

                        -- back to idle if all commands have been written 
                        if draw_counter >= command_pointer then  
                            state <= reading_data_state;
                            draw_counter := 0;
                            command_pointer := 0;
                        end if;       

                    -- if it is an invalid opcode then the fifo is probably full
                    -- so as a precaution just skip this command stack 
                    else 
                        draw_counter := 0;
                        command_pointer := 0;
                        state <= reading_data_state;
                    end if;     
                    
                when reading_data_state =>
                    vga_we <= '0';
                     -- when there is content in the fifo 
                    if fifo_empty = '0' then 
                        fifo_read_en <= '1';
                        state <= reading_data_state_2;
                    end if;
                when reading_data_state_2 =>
                    state <= reading_data_state_3;
                when reading_data_state_3 =>
                    command_stack(command_pointer) := fifo_data;
                    command_pointer := command_pointer + 1; 
                    state <= reading_data_state;
                    
                    fifo_words_read := fifo_words_read + 1;

                    state <= reading_data_state;
                    -- reset counter so next opcode can be read
                    if fifo_words_read >= 4 then 
                        fifo_words_read := 0;
                        state <= drawing_state;
                    end if;
                    
                    fifo_read_en <= '0';

            end case;                
        end if;
    end process;
    

end;