# clock uncertainty
derive_clock_uncertainty

# 50 MHz (20 ns) clock 
create_clock -name CLOCK_50 -period 20 [get_ports {CLOCK_50}]

# 25 mhz cpu core clock 
create_clock -name CLOCK_25 -period 40 [get_registers {CPU:cpu_inst|CLOCK_25}]

# 25 Mhz VGA Clock
create_generated_clock -name VGA_CLK -source [get_ports {CLOCK_50}] [get_ports {VGA_CLK}]

# fifo clocks
create_generated_clock -name fifo_clk -source [get_nets {cpu_inst|CLOCK_25}] [get_nets {cpu_inst|fifo_clock_signal}]
create_generated_clock -name vJTAG_clock -source [get_ports {altera_reserved_tck}] [get_nets {vJTAG_interface_inst|o_clock}]

# derive pll clocks
derive_pll_clocks -create_base_clocks


# input delay
set_input_delay -clock { CLOCK_50 } -min 0.7 [get_ports {KEY_in* reset_n}]
set_input_delay -clock { CLOCK_50 } -max 3.15 [get_ports {KEY_in* reset_n}]

# vJTAG
set_input_delay -clock { altera_reserved_tck } -min 0.5  [get_ports {altera_reserved_tdi altera_reserved_tms}]
set_input_delay -clock { altera_reserved_tck } -max 2.05  [get_ports {altera_reserved_tdi altera_reserved_tms}]


# output delay
set_output_delay -clock { VGA_CLK } -min 1 [get_ports {VGA*}]
set_output_delay -clock { VGA_CLK } -max 2.05 [get_ports {VGA*}]

set_output_delay -clock { CLOCK_50 } -min 1 [get_ports {segment* LED*}]
set_output_delay -clock { CLOCK_50 } -max 2.05 [get_ports {segment* LED*}]

# vJTAG 
set_output_delay -clock { altera_reserved_tck } -min 0.5  [get_ports {altera_reserved_tdo}]
set_output_delay -clock { altera_reserved_tck } -max 2.05  [get_ports {altera_reserved_tdo}]

#set_min_delay -from [get_registers {FIFO_stack:vJTAG_FIFO_inst|GrayCode:grayCode_write|gray_out*}] -to [get_registers {FIFO_stack:vJTAG_FIFO_inst|stack_empty}] 0
#set_max_delay -from [get_registers {FIFO_stack:vJTAG_FIFO_inst|GrayCode:grayCode_write|gray_out*}] -to [get_registers {FIFO_stack:vJTAG_FIFO_inst|stack_empty}] 10