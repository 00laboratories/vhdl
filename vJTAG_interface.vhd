--*****************************************************************************
-- vJTAG_interface.vhd
--
-- Author: Joakim Lövgren
--
-- Target Devices: ALTERA Cyclone V 5CSEMA5F31C6N
-- Tool versions: Quartus v16 and ModelSim
--
--	Description:
--		interface for the virtual JTAG  
--*****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vJTAG_interface is
    port (
        -- input 
        clock : in std_logic; 
        data_in : in std_logic; -- data in when shift state is true
        instruction_in : in std_logic; -- instruction 
        shift_state : in std_logic; -- indicates when we should shift input data
        update_state : in std_logic; -- indicates that data has been transmitted
        -- output
        tdo : out std_logic;
        out_data : out std_logic_vector(15 downto 0);
        out_clk : out std_logic;
        out_enable_fifo_write : out std_logic
    );
end entity vJTAG_interface;

architecture rtl of vJTAG_interface is
    -- so we always have data to send back 
    signal bypass_data : std_logic := '0';
    -- data register 
    signal data_reg : std_logic_vector(7 downto 0) := (others => '0'); 
    signal out_data_signal : std_logic_vector(15 downto 0):= (others => '0'); 
begin

    -- maintain continuity with data out 
    tdo <= data_reg(0) when instruction_in = '1' else bypass_data;

    out_clk <= clock;

    -- read data --
    process( clock, update_state )
        variable clock_count : integer := 0;
        begin
        if rising_edge( clock ) then 
            
            -- clock out data 
            if clock_count > 0 then      
                clock_count := clock_count - 1;
                out_enable_fifo_write <= '0'; -- disable write to fifo stack 
            end if;

            -- update bypass in case instruction in is not 1
            bypass_data <= data_in;
            
            if shift_state = '1' then 
                if instruction_in = '1' then 
                    -- shift data in and out 
                    data_reg <= std_logic_vector(unsigned(data_reg) sll 1);
                    data_reg(0) <= data_in;
                end if;
            elsif update_state = '1' and (clock_count = 0) then 
                clock_count := 1;
                out_enable_fifo_write <= '1'; -- enable write to fifo stack 
                out_data <= "00000000" & data_reg(7 downto 0);
            end if;       
        end if;
    end process;

end;